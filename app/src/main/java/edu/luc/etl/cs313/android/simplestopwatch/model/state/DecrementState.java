package edu.luc.etl.cs313.android.simplestopwatch.model.state;

import android.app.Activity;

import edu.luc.etl.cs313.android.simplestopwatch.R;
import edu.luc.etl.cs313.android.simplestopwatch.common.Constants;

class DecrementState extends Activity implements StopwatchState {

    public DecrementState(final StopwatchSMStateView sm) {
        this.sm = sm;
    }

    private final StopwatchSMStateView sm;

    @Override
    public void onButtonClick(int counter) {
        sm.actionStop();
        sm.toStoppedState();
        sm.setRuntime(0);
        sm.actionReset();
    }

    @Override
    public void onStartInputText(int counter) {
        sm.actionStop();
        sm.toStoppedState();
        sm.setRuntime(0);
        sm.actionReset();
    }

    @Override
    public void onTick() {
        sm.actionStartDec();
    }

    @Override
    public void onTickDec() {
        // decrement the timer until it is 00
        if (!sm.isEmpty()) {
            sm.actionDec();
            updateView();
        }
        // once it is 00, go to ALARM state
        else {
            sm.toAlarmState();
        }
    }

    @Override
    public void updateView() {
        sm.updateUIDecrementTime();
    }

    @Override
    public int getId() {
        return R.string.DECREMENT;
    }

    @Override
    public String getButtonName() {
        return Constants.BUTTON_NAME_CANCEL_TIMER;
    }

    @Override
    public String getInputText() {
        return "Please wait until 00...";
    }
}
