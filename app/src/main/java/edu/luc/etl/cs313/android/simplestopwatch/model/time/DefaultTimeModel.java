package edu.luc.etl.cs313.android.simplestopwatch.model.time;

import static edu.luc.etl.cs313.android.simplestopwatch.common.Constants.*;

/**
 * An implementation of the stopwatch data model.
 */
public class DefaultTimeModel implements TimeModel {

	private int runningTime = 0;

	private int runningTimeDec = 0;

	private int lapTime = -1;

    @Override
	public void resetRuntime() {
		runningTime = 0;
	}

    @Override
	public void incRuntime() {
		runningTime = runningTime + 1;
	}

	@Override
	public void decRuntime() {
		runningTimeDec = runningTimeDec - 1;
	}

    @Override
	public int getRuntime() {
		return runningTime;
	}

	@Override
	public int getRuntimeDec() {
		return runningTimeDec;
	}

	@Override
	public void setRuntime(int counter) {
		runningTimeDec = counter;
	}
}