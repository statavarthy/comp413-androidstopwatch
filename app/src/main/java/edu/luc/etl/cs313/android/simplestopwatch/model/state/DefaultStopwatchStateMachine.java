package edu.luc.etl.cs313.android.simplestopwatch.model.state;

import android.app.Activity;
import android.os.Bundle;

import edu.luc.etl.cs313.android.simplestopwatch.R;
import edu.luc.etl.cs313.android.simplestopwatch.common.StopwatchUIUpdateListener;
import edu.luc.etl.cs313.android.simplestopwatch.model.clock.ClockModel;
import edu.luc.etl.cs313.android.simplestopwatch.model.time.TimeModel;

/**
 * An implementation of the state machine for the stopwatch.
 *
 * @author laufer
 */
public class DefaultStopwatchStateMachine extends Activity implements StopwatchStateMachine {

    public DefaultStopwatchStateMachine(final TimeModel timeModel, final ClockModel clockModel) {
        this.timeModel = timeModel;
        this.clockModel = clockModel;
    }

    private final TimeModel timeModel;

    private final ClockModel clockModel;

    private int min = 0;

    /**
     * The internal state of this adapter component. Required for the State pattern.
     */
    private StopwatchState state;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
    }

    protected void setState(final StopwatchState state) {
        this.state = state;
        uiUpdateListener.updateState(state.getId());
    }

    private StopwatchUIUpdateListener uiUpdateListener;

    @Override
    public void setUIUpdateListener(final StopwatchUIUpdateListener uiUpdateListener) {
        this.uiUpdateListener = uiUpdateListener;
    }

    // forward event uiUpdateListener methods to the current state
    // these must be synchronized because events can come from the
    // UI thread or the timer thread
    @Override
    public synchronized void onButtonClick(int counter) {
        state.onButtonClick(counter);
    }

    @Override
    public synchronized void onStartInputText(int counter) {
        state.onStartInputText(counter);
    }

    @Override
    public synchronized void onTick() {
        state.onTick();
    }

    @Override
    public synchronized void onTickDec() {
        state.onTickDec();
    }


    @Override
    public void updateUIIncrementTime() {
        uiUpdateListener.updateTime(timeModel.getRuntime());
        uiUpdateListener.updateInputText(state.getInputText());
    }

    @Override
    public void updateUIDecrementTime() {
        uiUpdateListener.updateTime(timeModel.getRuntimeDec());
        uiUpdateListener.updateButtonName(state.getButtonName());
        uiUpdateListener.updateInputText(state.getInputText());
    }

    @Override
    public void updateUIAlarmTime() {
        uiUpdateListener.updateTime(timeModel.getRuntimeDec());
        uiUpdateListener.updateButtonName(state.getButtonName());
        uiUpdateListener.updateInputText(state.getInputText());
    }

    @Override
    public void updateUIStopTime() {
        uiUpdateListener.updateTime(timeModel.getRuntimeDec());
        uiUpdateListener.updateButtonName(state.getButtonName());
        uiUpdateListener.updateInputText(state.getInputText());
    }


    // known states
    private final StopwatchState STOPPED = new StoppedState(this);
    private final StopwatchState INCREMENT = new IncrementState(this);
    private final StopwatchState DECREMENT = new DecrementState(this);
    private final StopwatchState ALARM = new AlarmState(this);

    // transitions
    @Override
    public void toIncrementState() {
        setState(INCREMENT);
    }

    @Override
    public void toStoppedState() {
        setState(STOPPED);
        actionDisableEnableInputText(true);
        uiUpdateListener.updateInputText("");
    }

    @Override
    public void toDecrementState() {
        setState(DECREMENT);
    }

    @Override
    public void toAlarmState() {
        setState(ALARM);
    }

    // actions
    @Override
    public void actionInit() {
        toStoppedState();
        actionReset();
    }

    @Override
    public void actionReset() {
        timeModel.resetRuntime();
        actionUpdateView();
    }

    @Override
    public void actionStart() {
        clockModel.start();
    }

    @Override
    public void actionStartDec() {
        clockModel.startDec();
    }

    @Override
    public void actionStop() {
        clockModel.stop();
    }

    @Override
    public void actionDec() {
        timeModel.decRuntime();
        actionUpdateView();
    }

    @Override
    public void actionUpdateView() {
        state.updateView();
    }

    @Override
    public void actionDisableEnableInputText(boolean value) {
        uiUpdateListener.enableDisableInputText(value);
    }

    @Override
    public void setRuntime(int counter) {
        timeModel.setRuntime(counter);
    }

    @Override
    public boolean isEmpty() {
        return timeModel.getRuntimeDec() <= min;
    }

    @Override
    public void actionBeepSound() {
        uiUpdateListener.soundBeep();
    }

}

