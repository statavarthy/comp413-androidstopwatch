package edu.luc.etl.cs313.android.simplestopwatch.model.counter;

import java.io.Serializable;

/**
 * A bounded counter abstraction for the Android click counter.
 *
 * @author laufer
 */
public interface ClickCounterModel extends Serializable {

    /** Increments the counter value. Precondition: counter is not full. */
    void increment();

    /**
     * Returns the current counter value.
     *
     * @return the current counter value
     */
    int get();

    /**
     * Indicates whether the counter is full (at its maximum).
     *
     * @return whether the counter is full
     */
    boolean isFull();


    /**
     * Sets the value if the counter when set manually from user input area.
     *
     * @return whether the counter is full
     */
    void setValue(int value);
}
