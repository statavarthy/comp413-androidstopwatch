package edu.luc.etl.cs313.android.simplestopwatch.model.state;

import android.content.Context;

import edu.luc.etl.cs313.android.simplestopwatch.R;
import edu.luc.etl.cs313.android.simplestopwatch.common.Constants;

class StoppedState implements StopwatchState {

	public StoppedState(final StopwatchSMStateView sm) {
		this.sm = sm;
	}

	private final StopwatchSMStateView sm;

	@Override
	public void onButtonClick(int counter) {
		sm.actionStart();
		sm.toIncrementState();
	}

	@Override
	public void onStartInputText(int counter) {
		sm.actionDisableEnableInputText(false);
		sm.actionBeepSound();
		sm.setRuntime(counter);
		sm.toDecrementState();
		sm.actionStartDec();
	}

	@Override
	public void onTick() {
		sm.actionStartDec();
	}

	@Override
	public void onTickDec() {
		if(!sm.isEmpty()) {
			sm.actionDec();
			updateView();
			sm.toIncrementState();
		}
		else {
			sm.toStoppedState();
			sm.actionStop();
		}
	}

	@Override
	public void updateView() {
		sm.updateUIStopTime();
	}

	@Override
	public int getId() {
		return R.string.STOPPED;
	}

	@Override
	public String getButtonName() {
		return Constants.BUTTON_NAME_INCREASE_TIME;
	}

	@Override
	public String getInputText() {
		return "Enter Time here to set the Timer";
	}
}
