package edu.luc.etl.cs313.android.simplestopwatch.common;

/**
 * Constants for the time calculations used by the stopwatch.
 */
public final class Constants {

	public static int SEC_PER_TICK = 1;
	public static int SEC_PER_MIN  = 60;
	public static int SEC_PER_HOUR = 3600;
	public static int MAX_COUNT = 99;

	// Button Names in different states
	public static String BUTTON_NAME_START_TIMER = "Set Time";
	public static String BUTTON_NAME_INCREASE_TIME = "Increase Time";
	public static String BUTTON_NAME_CANCEL_TIMER = "Cancel Timer";

	private Constants() { }
}