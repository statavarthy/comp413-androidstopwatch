package edu.luc.etl.cs313.android.simplestopwatch.android;

import edu.luc.etl.cs313.android.simplestopwatch.model.counter.BoundedCounter;
import edu.luc.etl.cs313.android.simplestopwatch.model.counter.ClickCounterModel;

/** Adapter for using the CLI bounded counter as the Android click counter model. */
public class BoundedCounterWrapper implements ClickCounterModel {

    final BoundedCounter counter;

    public BoundedCounterWrapper(final BoundedCounter counter) {
        this.counter = counter;
    }

    @Override
    public void increment() {
        counter.increment();
    }


    @Override
    public int get() {
        return counter.get();
    }

    @Override
    public boolean isFull() {
        return counter.isFull();
    }

    @Override
    public void setValue(int value) {
        counter.setValue(value);

    }


}
