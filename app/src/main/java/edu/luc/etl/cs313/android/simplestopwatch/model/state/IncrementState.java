package edu.luc.etl.cs313.android.simplestopwatch.model.state;

import android.app.Activity;
import android.content.Context;

import edu.luc.etl.cs313.android.simplestopwatch.R;
import edu.luc.etl.cs313.android.simplestopwatch.common.Constants;

class IncrementState extends Activity implements StopwatchState {

	public IncrementState(final StopwatchSMStateView sm) {
		this.sm = sm;
	}

	int tick_count = 0;

	private final StopwatchSMStateView sm;

	@Override
	public void onButtonClick(int counter) {
		sm.actionDisableEnableInputText(false);
		sm.toIncrementState();
		tick_count = 0;
		sm.setRuntime(counter);

		// start the timer to decrement if the count reaches 99
		if(counter == Constants.MAX_COUNT) {
			sm.actionStop();
			sm.actionBeepSound();
			sm.toDecrementState();
			sm.actionStartDec();
		}
	}

	@Override
	public void onStartInputText(int counter) {
		throw new UnsupportedOperationException("onStartInputText");
	}

	@Override
	public void onTick() {
		// increase the tick_count variable to record 3 seconds count
		tick_count = tick_count + 1;

		// start the decrement when 3 seconds have elapsed
		if (tick_count == 3) {
			tick_count = 0;
			sm.actionBeepSound();
			sm.actionStop();
			sm.toDecrementState();
			sm.actionStartDec();
		}
	}

	@Override
	public void onTickDec() {
		throw new UnsupportedOperationException("onTickDec");
	}

	@Override
	public void updateView() {
		sm.updateUIIncrementTime();
	}

	@Override
	public int getId() {
		return R.string.RUNNING;
	}

	@Override
	public String getButtonName() {
		return Constants.BUTTON_NAME_INCREASE_TIME;
	}

	@Override
	public String getInputText() {
		return "Not available during this operation";
	}
}
