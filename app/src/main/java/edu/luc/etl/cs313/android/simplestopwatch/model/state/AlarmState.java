package edu.luc.etl.cs313.android.simplestopwatch.model.state;

import android.content.Context;

import edu.luc.etl.cs313.android.simplestopwatch.R;

class AlarmState implements StopwatchState {

    public AlarmState(final StopwatchSMStateView sm) {
        this.sm = sm;
    }

    private final StopwatchSMStateView sm;

    @Override
    public void onButtonClick(int counter) {
        sm.actionStop();
        sm.toStoppedState();
        sm.actionReset();
    }

    @Override
    public void onStartInputText(int counter) {
        sm.actionStop();
        sm.toStoppedState();
        sm.actionReset();
    }

    @Override
    public void onTickDec() {
        updateView();
        sm.actionBeepSound();
    }

    @Override
    public void onTick() {
        throw new UnsupportedOperationException("onTick");
    }

    @Override
    public void updateView() {
        sm.updateUIAlarmTime();
    }

    @Override
    public int getId() {
        return R.string.LAP_STOPPED;
    }

    @Override
    public String getButtonName() {
        return "Stop Alarm !";
    }

    @Override
    public String getInputText() {
        return "Please wait until Alarm stops...";
    }
}
