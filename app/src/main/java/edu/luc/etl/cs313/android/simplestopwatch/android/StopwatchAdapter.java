package edu.luc.etl.cs313.android.simplestopwatch.android;

import android.app.Activity;
import android.media.AudioManager;
import android.media.MediaPlayer;
import android.media.MediaPlayer.OnCompletionListener;
import android.media.RingtoneManager;
import android.net.Uri;
import android.os.Bundle;
import android.text.Editable;
import android.text.TextWatcher;
import android.util.Log;
import android.view.Gravity;
import android.view.KeyEvent;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import java.io.IOException;

import edu.luc.etl.cs313.android.simplestopwatch.R;
import edu.luc.etl.cs313.android.simplestopwatch.common.Constants;
import edu.luc.etl.cs313.android.simplestopwatch.common.StopwatchUIUpdateListener;
import edu.luc.etl.cs313.android.simplestopwatch.model.ConcreteStopwatchModelFacade;
import edu.luc.etl.cs313.android.simplestopwatch.model.StopwatchModelFacade;
import edu.luc.etl.cs313.android.simplestopwatch.model.counter.BoundedCounter;
import edu.luc.etl.cs313.android.simplestopwatch.model.counter.ClickCounterModel;

/**
 * A thin adapter component for the stopwatch.
 *
 * @author laufer
 */
public class StopwatchAdapter extends Activity implements StopwatchUIUpdateListener {

    private static String TAG = "stopwatch-android-activity";

    /**
     * The state-based dynamic counterModel.
     */
    private ClickCounterModel counterModel;

    private StopwatchModelFacade stopwatchModel;

    protected void setModel(final ClickCounterModel model, final StopwatchModelFacade sModel) {
        this.counterModel = model;
        this.stopwatchModel = sModel;
    }


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        // inject dependency on view so this adapter receives UI events
        setContentView(R.layout.activity_main);

        // align the input editable textarea to CENTER
        EditText inputTextArea = (EditText) findViewById(R.id.inputTime);
        inputTextArea.setGravity(Gravity.CENTER);

        // Set the ENTER key listener for the input text area
        inputTextArea.setOnKeyListener(new View.OnKeyListener() {
            public boolean onKey(View view, int keyCode, KeyEvent event) {
                // If the event is a key-down event on the "enter" button
                if ((event.getAction() == KeyEvent.ACTION_DOWN) && (keyCode == KeyEvent.KEYCODE_ENTER)) {

                    // call the functionality to start the timer after ENTER is pressed
                    if (!inputTextArea.getText().toString().equalsIgnoreCase("")) {
                        int inputText = Integer.parseInt(inputTextArea.getText().toString());
                        if (inputText > 0) {
                            onIncrement(view);
                            return true;
                        }
                    }
                }
                return false;
            }
        });

        // change the button name when text is entered in the user input
        TextWatcher watcher= new TextWatcher() {
            public void afterTextChanged(Editable s) { }
            public void beforeTextChanged(CharSequence s, int start, int count, int after) { }
            public void onTextChanged(CharSequence s, int start, int before, int count) {
                if (!inputTextArea.getText().toString().equals("")) {
                    updateButtonName(Constants.BUTTON_NAME_START_TIMER);
                }
            }
        };

        inputTextArea.addTextChangedListener(watcher);

        // inject dependency on counterModel into this so counterModel receives UI events
        this.setModel(createModelFromClassName(), new ConcreteStopwatchModelFacade());
        // inject dependency on this into counterModel to register for UI updates
        stopwatchModel.setUIUpdateListener(this);
    }

    @Override
    protected void onStart() {
        super.onStart();
        stopwatchModel.onStart();
    }


    /**
     * Creates a counterModel instance from the class name provided as the string value
     * of the external model_class resource.
     *
     * @return the counterModel instance
     */
    protected ClickCounterModel createModelFromClassName() {
        // catch checked exceptions
        try {
            // for flexibility, instantiate counterModel based on externally configured class name
            final BoundedCounter model = Class
                    .forName(getResources().getString(R.string.model_class))
                    .asSubclass(BoundedCounter.class)
                    .getConstructor(Integer.TYPE, Integer.TYPE)
                    .newInstance(0, 99);
            return new BoundedCounterWrapper(model);
        } catch (final Throwable ex) {
            Log.d(TAG, "checked exception while instantiating counterModel", ex);
            // re-throw as unchecked exception
            throw new RuntimeException(ex);
        }
    }

    /**
     * Updates the seconds area in the UI.
     *
     * @param time
     */
    public void updateTime(final int time) {
        // UI adapter responsibility to schedule incoming events on UI thread
        runOnUiThread(new Runnable() {
            @Override
            public void run() {
                final TextView tvS = (TextView) findViewById(R.id.seconds);
                tvS.setText(Integer.toString(time / 10) + Integer.toString(time % 10));
                counterModel.setValue(time);

                // disable the increment button when the counterModel (counter) is full
                findViewById(R.id.increment).setEnabled(!counterModel.isFull());
            }
        });
    }

    /**
     * Updates the state name in the UI.
     *
     * @param stateId
     */
    public void updateState(final int stateId) {
        // UI adapter responsibility to schedule incoming events on UI thread
        runOnUiThread(new Runnable() {
            @Override
            public void run() {
                final TextView stateName = (TextView) findViewById(R.id.stateName);
                stateName.setText(getString(stateId));
            }
        });
    }

    /**
     * (OPTIONAL) : Updates the button name in the UI.
     *  The button name changes as per the state
     * @param buttonName
     */
    public void updateButtonName(String buttonName) {
        // UI adapter responsibility to schedule incoming events on UI thread
        runOnUiThread(new Runnable() {
            @Override
            public void run() {
                //findViewById(R.layout.inr).setEnabled(true);
                final Button incrementButton = (Button) findViewById(R.id.increment);
                //testButton.setTag(1);
                incrementButton.setText(buttonName);
            }
        });
    }

    /**
     * Reset the input text once the timer is started through user input text
     *  (Through Virtual Keyboard)
     * @param txt
     */
    public void updateInputText(String txt) {
        // UI adapter responsibility to schedule incoming events on UI thread
        runOnUiThread(new Runnable() {
            @Override
            public void run() {
                final EditText inputText = (EditText) findViewById(R.id.inputTime);
                inputText.setText("");
                inputText.setHint(txt);
            }
        });
    }

    /**
     * This method disables/enables the input text area
     * if value = true, then enables the text area
     * if value = false, then disables the text area
     * @param value
     */
    public void enableDisableInputText(boolean value) {
        // UI adapter responsibility to schedule incoming events on UI thread
        runOnUiThread(new Runnable() {
            @Override
            public void run() {
                final EditText inputText = (EditText) findViewById(R.id.inputTime);
                inputText.setEnabled(value);

                // if enabled then background color is white
                if (value) {
                    inputText.setBackgroundColor(getResources().getColor(android.R.color.white));
                }
                // if disabled then background color is gray
                else {
                    inputText.setBackgroundColor(getResources().getColor(android.R.color.darker_gray));
                }
            }
        });
    }

    public void soundBeep() {
        final Uri defaultRingtoneUri = RingtoneManager.getDefaultUri(RingtoneManager.TYPE_NOTIFICATION);
        final MediaPlayer mediaPlayer = new MediaPlayer();

        try {
            mediaPlayer.setDataSource(getApplicationContext(), defaultRingtoneUri);
            mediaPlayer.setAudioStreamType(AudioManager.STREAM_NOTIFICATION);
            mediaPlayer.prepare();
            mediaPlayer.setOnCompletionListener(MediaPlayer::release);
            mediaPlayer.start();
            mediaPlayer.setOnCompletionListener(new OnCompletionListener() {
                public void onCompletion(MediaPlayer mp) {
                    mp.release();

                }

                ;
            });


        } catch (final IOException ex) {
            throw new RuntimeException(ex);
        }
    }

    /**
     * This method starts incrementing the counterModel
     * @param view
     */
    public void onIncrement(final View view) {
        final TextView valueView = (TextView) findViewById(R.id.seconds);
        final EditText inputValue = (EditText) findViewById(R.id.inputTime);
        counterModel.setValue(Integer.parseInt(valueView.getText().toString()));
        counterModel.increment();

        // Check if the user has input any value in the input text area
        if (!inputValue.getText().toString().equalsIgnoreCase("") && Integer.parseInt(inputValue.getText().toString()) >= 1) {
            counterModel.setValue(Integer.parseInt(inputValue.getText().toString()));
            updateView();
            stopwatchModel.onStartInputText(counterModel.get());
        }
        // if there is no user input
        else {
            updateView();
            stopwatchModel.onButtonClick(counterModel.get());
        }
    }

    /**
     * Updates the seconds field with the latest count
     */
    protected void updateView() {
        final TextView valueView = (TextView) findViewById(R.id.seconds);
        valueView.setText(Integer.toString(counterModel.get() / 10) + Integer.toString(counterModel.get() % 10));
        findViewById(R.id.increment).setEnabled(!counterModel.isFull());
    }
}
