package edu.luc.etl.cs313.android.simplestopwatch.common;

import android.content.Context;

/**
 * A listener for stopwatch events coming from the UI.
 *
 * @author laufer
 */
public interface StopwatchUIListener {
	public void onButtonClick(int counter);
	public void onStartInputText(int counter);

}
