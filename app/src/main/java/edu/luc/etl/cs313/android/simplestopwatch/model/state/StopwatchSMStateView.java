package edu.luc.etl.cs313.android.simplestopwatch.model.state;

import android.content.Context;

/**
 * The restricted view states have of their surrounding state machine.
 * This is a client-specific interface in Peter Coad's terminology.
 *
 * @author laufer
 */
interface StopwatchSMStateView {

	// transitions
	void toIncrementState();
	void toStoppedState();
	void toDecrementState();
	void toAlarmState();

	// actions
	void actionInit();
	void actionReset();
	void actionStart();
	void actionStop();
	void actionUpdateView();
	void actionStartDec();
	void actionDisableEnableInputText(boolean value);
	void actionDec();

	// state-dependent UI updates
	void updateUIIncrementTime();
	void updateUIDecrementTime();
	void updateUIAlarmTime();
	void updateUIStopTime();

	void setRuntime(int counter);
	boolean isEmpty();
	void actionBeepSound();
}
