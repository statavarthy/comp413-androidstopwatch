package edu.luc.etl.cs313.android.simplestopwatch.model.counter;

/** A bounded counter abstraction. */
// begin-interface-BoundedCounter
public interface BoundedCounter {
    void increment();
    void decrement();
    int get();
    boolean isFull();
    boolean isEmpty();
    void setValue(int value);

}
// end-interface-BoundedCounter
